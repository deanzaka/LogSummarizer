import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class FindErrors {
	ArrayList<String> list = new ArrayList<String>();
	ArrayList<Integer> times = new ArrayList<Integer>();
	ArrayList<String> dataError = new ArrayList<String>();
	ArrayList<String> combine = new ArrayList<String>();
	boolean newData, catchData;
	
	public FindErrors() {
		
	}
	
	public ArrayList<String> findErrors(String filename) {
		String line = null;
		
		BufferedReader bufferedReader = null;
		list.clear();
		times.clear();
		dataError.clear();
		combine.clear();
		try {
			FileReader fileReader = new FileReader(filename);
			bufferedReader = new BufferedReader(fileReader);
			
			catchData = false;
			while((line = bufferedReader.readLine()) != null) {
				if(line.contains("<ErrorMessage>")) {
					if(list.isEmpty()) {
						list.add(line);
						times.add(1);
						catchData = true;
					}
					else {
						for(int i = 0; i < list.size(); i++) {
							if(line.equals(list.get(i))) {
								newData = false;
								times.set(i, times.get(i) + 1);
								break;
							}
							else {
								newData = true;
							}
						}
						if(newData == true) {
							list.add(line);
							times.add(1);
							newData = false;
							catchData = true;
						}
					}
				}
				if(line.contains("TCRMService") && catchData == true) {
					dataError.add(line);
					catchData = false;
				}
			}
		} catch (FileNotFoundException ex) {
			System.out.println("File " + filename + " not found.");
		} catch (IOException ex) {
			System.out.println("Error reading file");
		} finally {
			try {
				if(bufferedReader != null) {
					bufferedReader.close();
				}
			} catch (IOException e) {
				
			}
		}
		
		for(int i = 0; i < list.size(); i++) {
			combine.add(list.get(i) + "," + times.get(i) + ",");
		}
		
		return combine;
	}
}
