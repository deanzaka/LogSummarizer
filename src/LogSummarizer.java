import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class LogSummarizer {


	public static void main(String[] args) {
		ArrayList<String> list = new ArrayList<String>();
		FindFile fFiles = new FindFile();
		FindErrors  fErrors = new FindErrors();
		list = fFiles.findFile("C:\\Users\\IBM_ADMIN\\Documents\\temp\\DELTA_OUTPUT", "batchLoadFail.out");
		
		BufferedWriter bufferedWriter = null;
		try {
			FileWriter fileWriter = new FileWriter("C:\\Users\\IBM_ADMIN\\Documents\\temp\\DELTA_OUTPUT\\generateErrors.csv");
			bufferedWriter = new BufferedWriter(fileWriter);
			
			for (String filename : list) {
				for (String error : fErrors.findErrors(filename)) {
					System.out.println(filename+","+error);
					bufferedWriter.write(filename+","+error);
					bufferedWriter.newLine();
					
				}
			}
		} catch (IOException e) {
			System.out.println("Error reading file");
		} finally {
			try {
				if(bufferedWriter != null) {
					bufferedWriter.close();
				}
			} catch (IOException e){
				
			}
		}
		
	}

}
